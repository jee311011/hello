FROM openjdk:17-jdk-slim

ADD target/Hello-0.0.1.jar Hello.jar

ENTRYPOINT ["java", "-jar","Hello.jar"]